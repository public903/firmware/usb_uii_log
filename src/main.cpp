#include <Arduino.h>
#include <avr/wdt.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_ADS1X15.h>

// #define DEBUG

Adafruit_ADS1115 ads;

unsigned long timer1PreviousMillis = 0;
const long timer200ms = 200; // Interval pre časovač (200 ms)

enum stav {
  VOLTAGE = 0,
  CURRENT1 = 1,
  CURRENT2 = 2
};

enum mode {
  NASTAVOVANIE = 0,
  MERANIE = 1,
};

struct merac {
  uint16_t voltage_raw;
  uint16_t current1_raw;
  uint16_t current2_raw;
  float voltage;
  float current1;
  float current2;
  stav active;
  mode measure = NASTAVOVANIE;
};

merac usbva;

#define MAX_BUF_SIZE 32 // Maximálna veľkosť bufferu

char uartbuf[MAX_BUF_SIZE]; // Buffer na uchovávanie prijatých dát
int uartbufIndex = 0; // Index do bufferu

void setup() {
  Serial.begin(115200);

  // Inicializácia ADS1115
  if (!ads.begin()) {
    Serial.println("Nepodarilo sa inicializovať ADS1115!");
    while (1);
  }

  // Zinicializované
  Serial.println("Podarilo sa inicializovat ADS1115!");

  // Nastavenie data rate na 8 sps (samples per second)
  ads.setDataRate(RATE_ADS1115_8SPS);
  ads.setGain(GAIN_TWO);  // +-2.048V x 10

  // Zapnutie watchdog timeru s časovačom na 2 sekundy
  wdt_enable(WDTO_2S);
}

void loop() {
  wdt_reset();

  if (Serial.available() > 0) { // Ak sú k dispozícii nejaké dáta na sériovom porte
    char receivedChar = Serial.read(); // Čítanie znaku zo sériového portu

    // Uloženie znaku do bufferu, ak nie je dosiahnutá maximálna veľkosť
    if (uartbufIndex < MAX_BUF_SIZE - 1) {
      uartbuf[uartbufIndex] = receivedChar;
      uartbufIndex++;
      uartbuf[uartbufIndex] = '\0'; // Null-terminátor na ukončenie reťazca
    }

    // Ak bol prijatý koniec riadka ('\n') alebo nový riadok ('\r')
    if (receivedChar == '\n' || receivedChar == '\r') {
      // Spracovanie prijatého reťazca
      uartbuf[uartbufIndex-1] = '\0';
      #ifdef DEBUG
        Serial.print("Prijaty retazec: ");
        Serial.println(uartbuf);
      #endif

      if (strcmp(uartbuf, "U?") == 0) {
        // Ak sa reťazec rovná "U?", poslať odpoveď na sériový port
        Serial.print("U=");
        // Tu môžete pridať hodnotu napätia (voltage) podľa vašich potrieb
        Serial.println(usbva.voltage); // Príklad hodnoty napätia
      } else if (strcmp(uartbuf, "I1?") == 0) {
        // Ak sa reťazec rovná "U?", poslať odpoveď na sériový port
        Serial.print("I1=");
        // Tu môžete pridať hodnotu napätia (voltage) podľa vašich potrieb
        Serial.println(usbva.current1); // Príklad hodnoty napätia
      } else if (strcmp(uartbuf, "I2?") == 0) {
        // Ak sa reťazec rovná "U?", poslať odpoveď na sériový port
        Serial.print("I2=");
        // Tu môžete pridať hodnotu napätia (voltage) podľa vašich potrieb
        Serial.println(usbva.current2); // Príklad hodnoty napätia
      }

      // Resetovanie bufferu
      uartbufIndex = 0;
      memset(uartbuf, 0, sizeof(uartbuf));
    }
  }

  // Časovač (200 ms)
  if (millis() - timer1PreviousMillis >= timer200ms) {
    timer1PreviousMillis = millis();  // Uloženie aktuálneho času
    // Váš kód pre časovač 200ms
    if (usbva.measure == NASTAVOVANIE) { // jeden cyklus nastav vstupy, druhý cyklus meraj, aby sa ustalili napatia na vstupoch
      if (ads.conversionComplete()) {   // teraz nastav vstupy
        switch (usbva.active)
        {
        case VOLTAGE:
          usbva.voltage_raw = ads.getLastConversionResults();           // uloz nameranu hodnotu z kanala VOLTAGE
          usbva.voltage = 10 * ads.computeVolts(usbva.voltage_raw);     // prepocitaj na napatie
          usbva.active = CURRENT1;                                      // prepni na dalsi kanal
          ads.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_1_3, false);  // prepni vstupy
          #ifdef DEBUG
          Serial.print("Uraw="); Serial.print(usbva.voltage_raw); Serial.print(" U="); Serial.print(usbva.voltage); Serial.println(" V");
          #endif
          break;

        case CURRENT1:
          usbva.current1_raw = ads.getLastConversionResults();           // uloz nameranu hodnotu z kanala VOLTAGE
          usbva.current1 = 10 * ads.computeVolts(usbva.current1_raw);     // prepocitaj na prud
          usbva.active = CURRENT2;                                      // prepni na dalsi kanal
          ads.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_2_3, false); // prepni vstupy
          #ifdef DEBUG
          Serial.print("I1raw="); Serial.print(usbva.current1_raw); Serial.print(" I1="); Serial.print(usbva.current1); Serial.println(" A");
          #endif
          break;

        case CURRENT2:
          usbva.current2_raw = ads.getLastConversionResults();           // uloz nameranu hodnotu z kanala VOLTAGE
          usbva.current2 = 10 * ads.computeVolts(usbva.current2_raw);     // prepocitaj na prud
          usbva.active = VOLTAGE;                                      // prepni na dalsi kanal
          ads.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, false);  // prepni vstupy
          #ifdef DEBUG
          Serial.print("I2raw="); Serial.print(usbva.current2_raw); Serial.print(" I2="); Serial.print(usbva.current2); Serial.println(" A");
          Serial.println();
          #endif
          break;

        default:
          break;
        }
        usbva.measure = MERANIE;   // v dalsom cyklu spusti meranie
      }
    } else if (usbva.measure == MERANIE) {  // teraz zacni merat
        if (ads.conversionComplete()) {     // ak aktualne neprebieha meranie
          switch (usbva.active)
          {
          case VOLTAGE:
            ads.startADCReading(ADS1X15_REG_CONFIG_MUX_SINGLE_0, false); // spusti meranie
            break;

          case CURRENT1:
            ads.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_1_3, false); // spusti meranie
            break;

          case CURRENT2:
            ads.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_2_3, false); // spusti meranie
            break;

          default:
            break;
          }
        usbva.measure = NASTAVOVANIE;   // v dalsom cyklu nastav vstupy
      }
    }
  }
}

